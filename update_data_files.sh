#!/bin/bash

for ii in 'awakenings' 'leader_skills' 'monsters'; do
    wget https://www.padherder.com/api/$ii
    cat $ii | python -m json.tool > tempfile
    mv tempfile static/${ii}.json
    rm $ii
done



