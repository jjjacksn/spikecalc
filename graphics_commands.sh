#!/bin/bash

# Note: you will need to have ImageMagick installed for this to work.
function create_disabled_icon() {
    local FILE="${1%*.png}"

    # create a matching size file that is all gray
    convert -size 29x30 xc:"#999999" gray32.png

    # blend the gray file and the icon
    composite -dissolve 50% "${FILE}.png" gray32.png "${FILE}_intermediate.png"

    # create a mask with rounded corners
    convert -size 29x30 xc:none -draw "roundrectangle 0,0,29,30,5,5" mask.png
    convert "${FILE}_intermediate.png" -matte mask.png \
        -compose DstIn -composite "${FILE}_disabled.png"

    # clean up
    rm -f gray32.png
    rm -f mask.png
    rm -f "${FILE}_intermediate.png"
}