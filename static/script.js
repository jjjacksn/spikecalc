function PADData(){
	this.monster_list = null;
	this.leader_skills = null;
	this.awakenings = null;
	this.data_loaded = 0;
	this.source = null;
	/*
	1:  Enhanced HP, 2: Enhanced Attack, 3: Enhanced Heal, 4: Reduce Fire Damage, 5: Reduce Water Damage, 6: Reduce Wood Damage, 7: Reduce Light Damage,
	8:  Reduce Dark Damage, 9: Auto-Recover, 10: Resistance-Bind, 11: Resistance-Dark, 12: Resistance-Jammers, 13: Resistance-Poison, 14: Enhanced Fire Orbs,
	15: Enhanced Water Orbs, 16: Enhanced Wood Orbs, 17: Enhanced Light Orbs, 18: Enhanced Dark Orbs, 19: Extend Time, 20: Recover Bind, 21: Skill Boost,
	22: Enhanced Fire Att., 23: Enhanced Water Att., 24: Enhanced Wood Att., 25: Enhanced Light Att., 26: Enhanced Dark Att., 27: Two-Prong Attack, 28: Resistance-Skill Lock,
	29: Enhanced Heart Orbs, 30: Multi Boost, 31: Dragon Killer, 32: God Killer, 33: Devil Killer, 34: Machine Killer,
	*/

	this.loadData = function(){
		this.data_loaded = 0;
		this.source = 'local';
		$.ajax('leader_skills.json', {dataType: 'json', context: this, success: function(a, b, c){this.leader_skills = a; this.data_loaded++;}});
		$.ajax('awakenings.json', {dataType: 'json', context: this, success: function(a, b, c){this.awakenings = a; this.data_loaded++;}});
		$.ajax('monsters.json', {dataType: 'json', context: this, success: function(a, b, c){this.monster_list = a; this.data_loaded++;}});
	}

	this.loadDataFromPADHerder = function(){
		this.data_loaded = 0;
		this.source = 'PADherder';
		$.ajax('https://www.padherder.com/api/leader_skills/', {dataType: 'json', context: this, success: function(a, b, c){this.leader_skills = a; this.data_loaded++;}});
		$.ajax('https://www.padherder.com/api/awakenings/', {dataType: 'json', context: this, success: function(a, b, c){this.awakenings = a; this.data_loaded++;}});
		$.ajax('https://www.padherder.com/api/monsters/', {dataType: 'json', context: this, success: function(a, b, c){this.monster_list = a; this.data_loaded++;}});
	}

	this.dataLoaded = function(){
		return (this.data_loaded == 3);
	}

	this.getMonster = function(id){
		var index = id < this.monster_list.length ? id : this.monster_list.length - 1;
		var monster = this.monster_list[index];
		while (monster.id != id){
			diff = monster.id - index;
			index = diff > 0 ? index - 1 : index + 1;
			monster = this.monster_list[index];
		}
		return monster;
	}
	
	this.getLeaderSkill = function(name){
		for (var i = 0; i < this.leader_skills.length; i++){
                    if (this.leader_skills[i].name == name) { return this.leader_skills[i].data; }
		}
	}
}

function Monster(monster, herder){
	if (herder == undefined || !herder){ return; }
	
	this.name = null;
	this.xp_curve = null;
	this.max_level = null;
	this.max_evo = null;
	this.id = this.original_id = null;
	this.level = this.current_xp = this.original_xp = null;

	this.type = [];
	this.element = [];
	
	this.lead = null;
	this.image = null;

	this.all_awakenings = this.awakenings = this.original_awakenings = [];
	this.awake = true;

	this.hp = this.hp_growth = this.plus_hp = null;
	this.atk = this.atk_growth = this.plus_atk = null;
	this.rcv = this.rcv_growth = this.plus_rcv = null;
	this.original_pluses = null;
	this.multiplier = [];

        this.latents = [];
        this.bound = false;
        this.original_pluses =  {'hp': 0, 'atk': 0, 'rcv': 0};

	this.xpAtLevel = function(level){
		return Math.round(this.xp_curve * Math.pow((level-1)/98, 2.5));
	}

	this.levelFromXP = function(XP){
		if (XP == this.xp_curve) { return this.max_level; }
		return 1 + Math.floor(Math.floor(this.max_level - 1) * Math.pow((XP/Math.max(this.xpAtLevel(this.max_level), 1)), 1/2.5));
		/*var num = Math.floor((this.max_level - 1) * Math.pow((XP/Math.max(this.xpAtLevel(this.max_level), 1)), 1/2.5) * 1000000);
		for (var i = 1; i <= 6; num = Math.round(num / 10), i++); // This should increase the precision and avoid those weird rounding bugs on certain XP curve/level combinations.
		return 1 + num;*/
	}

	this.countAwakening = function(id){
		if (!this.awake){ return 0 }
		var cnt = 0;
		var awkn = this.awakenings;
		while (awkn.indexOf(id) != -1){
			cnt++;
			awkn = awkn.slice( awkn.indexOf(id)+1 );
		}
		return cnt;
	}

        this.countLatents = function(stat) {
            var count = 0;
            for (var ii = 0; ii < this.latents.length; ii++) {
                var stat_index = {'hp' : 1, 'atk' : 2, 'rcv' : 3}[stat];
                if (this.latents[ii] == stat_index)
                    count++;
            }
            return count;
        }

	this.calcStat = function(stat){
		// MinStat + (MaxStat - MinStat) * (Level-1/MaxLevel-1)^StatGrowth
		var mods = this[stat + '_growth'];
		var plus_mult = {'hp': 10, 'atk': 5, 'rcv': 3}[stat];
		var awoke_id = {'hp': 1, 'atk': 2, 'rcv': 3}[stat];
		var awoke_mult = {'hp': 200, 'atk': 100, 'rcv': 50}[stat];
                var latent_mult = {'hp': 0.015, 'atk': 0.01, 'rcv': 0.04};
                var base_stat = mods['min'] + ((mods['max']-mods['min']) * Math.pow((this.level-1)/Math.max(this.max_level-1, 1), mods['scale']));

                var awaken_stat = this.countAwakening(awoke_id) * awoke_mult;
                var plus_stat = this['plus_' + stat] * plus_mult;
                var latent_multiplier = 1 + (this.countLatents(stat) * latent_mult[stat]);
                var total_stat = latent_multiplier * (base_stat + awaken_stat) + plus_stat;

		return total_stat;
	}

	this.getStatsUnderLeaders = function(leader, friend, extra){
		this.multiplier = [1, 1, 1];
		if (leader == undefined || !leader){ leader = [1, 1, 1]; }
		if (friend == undefined || !friend){ friend = [1, 1, 1]; }
		if (extra == undefined || !extra){ extra = []; }

		var stats = [this.calcStat('hp'), this.calcStat('atk'), this.calcStat('rcv')];

		function helperCalc(stat_array, skill){
			var stats = stat_array;
			if (skill.length < 4){
				stats = [stats[0] * skill[0], stats[1] * skill[1], stats[2] * skill[2]];
				this.multiplier = [this.multiplier[0] * skill[0], this.multiplier[1] * skill[1], this.multiplier[2] * skill[2]];
			}
			else {
				for (var i = 3; i < skill.length; i++){
					var constraint = skill[i][0] == 'elem' ? 'element' : 'type';
					var intersect = this[constraint].filter(function(n){ return skill[i].indexOf(n) != -1; });
					if (intersect.length){
						stats = [stats[0] * skill[0], stats[1] * skill[1], stats[2] * skill[2]];
						this.multiplier = [this.multiplier[0] * skill[0], this.multiplier[1] * skill[1], this.multiplier[2] * skill[2]];
						break;
					}
				}
			}
			return stats;
		}

		stats = helperCalc.call(this, stats, leader);
		stats = helperCalc.call(this, stats, friend);
		for (var i = 0; i < extra.length; i++){
			if (extra[i])
				stats = helperCalc.call(this, stats, extra[i]);
		}

		return stats;
	}

	this.checkActiveConditions = function(skill, current_element){
		if (skill == undefined || !skill){ return false; }
		if (skill.length < 4){ return true; }
		for (var i = 3; i < skill.length; i++){
			var constraint = skill[i][0] == 'elem' ? 'element' : 'type';
			if (constraint == 'element') {
				// Element active skills only return true if you're attacking with that element at the time.
				if ( skill[i].indexOf(current_element) != -1 ){ return true; }
			}
			else {
				var intersect = this[constraint].filter(function(n){ return skill[i].indexOf(n) != -1; });
				if (intersect.length){ return true; }
			}
		}
		return false;
	}

	this.initMonster = function(data){
		this.id = this.original_id = data.monster;
		var monsterFromHerd = herder.getMonster(this.id);
		if (!monsterFromHerd) { return; }
		
		this.name = monsterFromHerd.name;
		this.xp_curve = monsterFromHerd.xp_curve;
		this.max_level = monsterFromHerd.max_level;
		this.max_evo = (data.target_evolution) ? (data.target_evolution) : this.original_id;
		this.current_xp = this.original_xp = data.current_xp != undefined ? data.current_xp : this.xp_curve;
		this.level = data.level == undefined ? this.levelFromXP(this.current_xp) : data.level;

		this.all_awakenings = monsterFromHerd.awoken_skills;
		this.awakenings = this.all_awakenings.slice(0, data.current_awakening || 0);
		this.original_awakenings = this.awakenings.length;
		this.awake = true;

		this.type = [monsterFromHerd.type];
		if (monsterFromHerd.type2) { this.type.push(monsterFromHerd.type2); }
		if (monsterFromHerd.type3) { this.type.push(monsterFromHerd.type3); }

		this.element = monsterFromHerd.element2 != undefined ? [monsterFromHerd.element, monsterFromHerd.element2] : [monsterFromHerd.element];

		this.lead = monsterFromHerd.leader_skill;
		this.image = "http://www.padherder.com/" + monsterFromHerd.image60_href;

		this.hp_growth = {'min': monsterFromHerd.hp_min, 'max': monsterFromHerd.hp_max, 'scale': monsterFromHerd.hp_scale};
		this.atk_growth = {'min': monsterFromHerd.atk_min, 'max': monsterFromHerd.atk_max, 'scale': monsterFromHerd.atk_scale};
		this.rcv_growth = {'min': monsterFromHerd.rcv_min, 'max': monsterFromHerd.rcv_max, 'scale': monsterFromHerd.rcv_scale};

		this.plus_hp = data.plus_hp || 0;
		this.plus_atk = data.plus_atk || 0;
		this.plus_rcv = data.plus_rcv || 0;
		this.original_pluses = {'hp': this.plus_hp, 'atk': this.plus_atk, 'rcv': this.plus_rcv};

		this.hp = this.calcStat('hp');
		this.atk = this.calcStat('atk');
		this.rcv = this.calcStat('rcv');

		this.multiplier = [1, 1, 1];

		for (var ii = 1; ii < 6; ii++) {
			var latent = data['latent' + ii];
			if (latent && latent != 0)
				this.latents.push(latent);
		}
	}

	this.updateMonster = function(data){
		this.id = data.monster != undefined ? data.monster : this.id;
		this.original_id = this.id;
		var monsterFromHerd = herder.getMonster(this.id);
		if (!monsterFromHerd) { return; }

		this.name = monsterFromHerd.name;
		this.xp_curve = monsterFromHerd.xp_curve;
		this.max_level = monsterFromHerd.max_level;
		this.max_evo = data.target_evolution ? data.target_evolution : this.id;
		if (data.level != undefined) { this.level = data.level; } // If the level is set, use that.
		else { // If the level isn't set, base it off the XP.
			if (data.current_xp != undefined){ this.current_xp = data.current_xp; }
			else { this.current_xp = this.xp_curve; } // If XP wasn't set, default to max level.
			this.level = this.levelFromXP(this.current_xp);
		}
		//this.current_xp = data.current_xp != undefined ? data.current_xp : this.xp_curve;
		//this.level = data.level != undefined ? data.level : this.levelFromXP(this.current_xp);

		this.all_awakenings = monsterFromHerd.awoken_skills;
		this.awakenings = this.all_awakenings.slice(0, data.current_awakening != undefined ? data.current_awakening : this.awakenings.length);
		this.awake = true;

		this.type = [monsterFromHerd.type];
		if (monsterFromHerd.type2) { this.type.push(monsterFromHerd.type2); }
		if (monsterFromHerd.type3) { this.type.push(monsterFromHerd.type3); }

		this.element = monsterFromHerd.element2 != undefined ? [monsterFromHerd.element, monsterFromHerd.element2] : [monsterFromHerd.element];

		this.lead = monsterFromHerd.leader_skill;
		this.image = "http://www.padherder.com/" + monsterFromHerd.image60_href;

		this.hp_growth = {'min': monsterFromHerd.hp_min, 'max': monsterFromHerd.hp_max, 'scale': monsterFromHerd.hp_scale};
		this.atk_growth = {'min': monsterFromHerd.atk_min, 'max': monsterFromHerd.atk_max, 'scale': monsterFromHerd.atk_scale};
		this.rcv_growth = {'min': monsterFromHerd.rcv_min, 'max': monsterFromHerd.rcv_max, 'scale': monsterFromHerd.rcv_scale};

		this.plus_hp = data.plus_hp != undefined ? data.plus_hp : this.plus_hp;
		this.plus_atk = data.plus_atk != undefined ? data.plus_atk : this.plus_atk;
		this.plus_rcv = data.plus_rcv != undefined ? data.plus_rcv : this.plus_rcv;

		this.hp = this.calcStat('hp');
		this.atk = this.calcStat('atk');
		this.rcv = this.calcStat('rcv');

		this.multiplier = [1, 1, 1];
	}

	if (monster)
		this.initMonster(monster);
}

function Team(team, herder){
	if (herder == undefined || !herder){ return; }

	this.name = null;
	this.data_loaded = 5;
	this.leader_skill = [1, 1, 1];
	this.friend_skill = [1, 1, 1];
	this.extra_skill = null;
	this.monsters = [null, null, null, null, null, null];
	this.row_enhance = [null, null, null, null, null, null];
	this.plus_orbs = [null, null, null, null, null, null];
	this.skill_boost = this.skill_bind_resist = this.blind_resist = this.jammer_resist = this.poison_resist = this.finger = null;

	this.buildTeam = function(data){
		this.name = data.name;
		this.data_loaded = 5;
		this.addMonsterToSlot(data.leader, 0);
		for (var i = 1; i <= 4; i++){
			var index = 'sub' + i;
			if (data[index]){ this.addMonsterToSlot(data[index], i); }
			else { this.monsters[i] = null; this.data_loaded--; }
		}

		if (data.friend_leader){
			monster = {'monster': data.friend_leader, 'level': data.friend_level, 'current_awakening': data.friend_awakening, 'plus_hp': data.friend_hp, 'plus_atk': data.friend_atk, 'plus_rcv': data.friend_rcv};
			for (var i = 1; i <= 6; i++){
				monster['latent'+i] = data['friend_latent'+i];
			}
			this.monsters[5] = new Monster(monster, herder);
			this.calcTeamStats();
		}
	}

	this.addMonsterToSlot = function(monsterID, slot){
		$.ajax( "https://www.padherder.com/user-api/monster/" + monsterID + "/", {dataType: 'json', context: this, success: function(a, b, c){ 
			this.monsters[slot] = new Monster(a, herder);
			this.data_loaded--;
			this.calcTeamStats();
		}});
	}

	this.calcTeamStats = function(){
		if (this.monsters[0]) { this.leader_skill = herder.getLeaderSkill(this.monsters[0].lead); }
		if (this.monsters[5]) { this.friend_skill = herder.getLeaderSkill(this.monsters[5].lead); }

		this.row_enhance = [null, null, null, null, null, null];
		this.plus_orbs = [null, null, null, null, null, null];
		this.skill_boost = this.skill_bind_resist = this.blind_resist = this.jammer_resist = this.poison_resist = this.finger = null;
	
		for (var i = 0; i < 6; i++){
			if ( this.monsters[i] == undefined || !this.monsters[i] ) { continue; }
			this.row_enhance[0] += this.monsters[i].countAwakening(22);
			this.row_enhance[1] += this.monsters[i].countAwakening(23);
			this.row_enhance[2] += this.monsters[i].countAwakening(24);
			this.row_enhance[3] += this.monsters[i].countAwakening(25);
			this.row_enhance[4] += this.monsters[i].countAwakening(26);
			this.row_enhance[5] += this.monsters[i].countAwakening(20);

			this.plus_orbs[0] += this.monsters[i].countAwakening(14);
			this.plus_orbs[1] += this.monsters[i].countAwakening(15);
			this.plus_orbs[2] += this.monsters[i].countAwakening(16);
			this.plus_orbs[3] += this.monsters[i].countAwakening(17);
			this.plus_orbs[4] += this.monsters[i].countAwakening(18);
			this.plus_orbs[5] += this.monsters[i].countAwakening(29);

			this.skill_boost += this.monsters[i].countAwakening(21);
			this.skill_bind_resist += this.monsters[i].countAwakening(28);
			this.blind_resist += this.monsters[i].countAwakening(11);
			this.jammer_resist += this.monsters[i].countAwakening(12);
			this.poison_resist += this.monsters[i].countAwakening(13);
			this.finger += this.monsters[i].countAwakening(19);
		}
		updateLeaderSkills();
	}

	this.dataLoaded = function(){ return (this.data_loaded == 0); }

	if (team)
		this.buildTeam(team);
}

function buildTable(){
	$('#team-block').addClass('hidden');
	$('#team-name').text(userTeam.name);
	var team_hp = team_rcv = 0;
        var dark_resists = 0;
        var leader_data = userTeam.leader_skill;
        var friend_data = userTeam.friend_skill;

	for (var i = 0; i <= 5; i++){
		if (!userTeam.monsters[i]){
			$('#id-row th.team-pos' + (i+1) + ' span').text('Click to edit!');
			$('.team-img-' + (i+1)).attr('src', 'https://www.padherder.com/static/img/monsters/60x60/0.png'); // Use ? image.
			$('#name-row td.team-pos' + (i+1) + ' span').text('');
			$('#level-row td.team-pos' + (i+1) + ' span').text('');
			$('#type-row td.team-pos' + (i+1) + ' span').html('');

			$('#hp-row .team-pos' + (i+1) + ' span').text('');
			$('#hp-row .team-pos' + (i+1) + ' span + span').text('');
			$('#atk-row .team-pos' + (i+1) + ' span').text('');
			$('#atk-row .team-pos' + (i+1) + ' span + span').text('');
			$('#rcv-row .team-pos' + (i+1) + ' span').text('');
			$('#rcv-row .team-pos' + (i+1) + ' span + span').text('');
			
			$('#lead-row td.team-pos' + (i+1) + ' span').text('');
			$('#awk-row td.team-pos' + (i+1) + ' span').text('');
			$('#awk-row-2 td.team-pos' + (i+1)).text('');
			$('#latent-row-2 td.team-pos' + (i+1)).text('');
			continue;
		}

		$('#id-row th.team-pos' + (i+1) + ' span').text(userTeam.monsters[i].id);
		$('.team-img-' + (i+1)).attr('src', userTeam.monsters[i].image);
		$('#name-row td.team-pos' + (i+1) + ' span').text(userTeam.monsters[i].name);
		$('#level-row td.team-pos' + (i+1) + ' span').text(userTeam.monsters[i].level);
		
		// Stack the types vertically.
		var type_string = "";
		for (var j = 0; j < userTeam.monsters[i].type.length; j++){
			type_string += types[userTeam.monsters[i].type[j]] + "<br/>";
		}

		var hp = Math.round( userTeam.monsters[i].getStatsUnderLeaders(leader_data, friend_data, userTeam.extra_skill)[0] );
		team_hp += hp;

		var rcv = Math.round( userTeam.monsters[i].getStatsUnderLeaders(leader_data, friend_data, userTeam.extra_skill)[2] );
		team_rcv += rcv;
		$('#hp-row .team-pos' + (i+1) + ' span').text( hp );
		$('#hp-row .team-pos' + (i+1) + ' span').tooltip('hide').attr('title', Math.round(userTeam.monsters[i].hp)).tooltip('fixTitle');
		$('#hp-row .team-pos' + (i+1) + ' span + span').text('(+' + userTeam.monsters[i].plus_hp + ')');

		$('#atk-row .team-pos' + (i+1) + ' span').text( Math.round( userTeam.monsters[i].getStatsUnderLeaders(leader_data, friend_data, userTeam.extra_skill)[1] ) );
		$('#atk-row .team-pos' + (i+1) + ' span').tooltip('hide').attr('title', Math.round(userTeam.monsters[i].atk)).tooltip('fixTitle');
		$('#atk-row .team-pos' + (i+1) + ' span + span').text('(+' + userTeam.monsters[i].plus_atk + ')');

		$('#rcv-row .team-pos' + (i+1) + ' span').text( rcv );
		$('#rcv-row .team-pos' + (i+1) + ' span').tooltip('hide').attr('title', Math.round(userTeam.monsters[i].rcv)).tooltip('fixTitle');
		$('#rcv-row .team-pos' + (i+1) + ' span + span').text('(+' + userTeam.monsters[i].plus_rcv + ')');

                // count latent dark resists
                if (userTeam.monsters[i].latents)
                    for (var jj = 0; jj < userTeam.monsters[i].latents.length; jj++)
                        if (userTeam.monsters[i].latents[jj] == 10)
                            dark_resists++;

                // calculate Hera preemptive damage -- maybe generalize this for arbitrary preemptives?
                var hera_damage = 38910 * (1 - 0.01 * dark_resists);
                $('#hera-damage').text(Math.round(hera_damage));

		// Display multipliers.
		$('#lead-row td.team-pos' + (i+1) + ' span').text("x" + (userTeam.monsters[i].multiplier[0] % 1 ? userTeam.monsters[i].multiplier[0].toFixed(2) : userTeam.monsters[i].multiplier[0])
                                                                  + " x" + (userTeam.monsters[i].multiplier[1] % 1 ? userTeam.monsters[i].multiplier[1].toFixed(2) : userTeam.monsters[i].multiplier[1])
                                                                  + " x" + (userTeam.monsters[i].multiplier[2] % 1 ? userTeam.monsters[i].multiplier[2].toFixed(2) : userTeam.monsters[i].multiplier[2]) );

		$('#type-row td.team-pos' + (i+1) + ' span').html(type_string);
		$('#awk-row td.team-pos' + (i+1) + ' span').text(userTeam.monsters[i].awakenings.length);
		
		// Awakening images.
		$('#awk-row-2 td.team-pos' + (i+1)).text('');
		for (var j = 0; j < userTeam.monsters[i].awakenings.length; j++){
			$('#awk-row-2 td.team-pos' + (i+1)).append( ("<img src=images/" + userTeam.monsters[i].awakenings[j] + (awoken_disabled ? "_disabled" : "") + ".png />") );
		}

		// Latent awakening images
		$('#latent-row-2 td.team-pos' + (i + 1)).text('');
		for (var jj = 0; jj < userTeam.monsters[i].latents.length; jj++) {
			var latent_number = userTeam.monsters[i].latents[jj];

			// seems pretty hacky.
			var image_number = {1 : 1, 2 : 2, 3 : 3, 4 : 19, 5 : 9, 6 : 4, 7 : 5, 8 : 6, 9 : 7, 10 : 8, 11: 28}[latent_number];

			$('#latent-row-2 td.team-pos' + (i+1)).append("<img src=images/" + image_number + ".png />");
		}
	}

	$('#hp-total').text(team_hp);
	$('#rcv-total').text(team_rcv);

	$('input[name=fr-enhance]').val(userTeam.row_enhance[0]);
	$('input[name=wt-enhance]').val(userTeam.row_enhance[1]);
	$('input[name=wd-enhance]').val(userTeam.row_enhance[2]);
	$('input[name=lt-enhance]').val(userTeam.row_enhance[3]);
	$('input[name=dk-enhance]').val(userTeam.row_enhance[4]);
	$('input[name=bc-view]').val(userTeam.row_enhance[5]);

	$('input[name=fr-plus]').val(userTeam.plus_orbs[0]);
	$('input[name=wt-plus]').val(userTeam.plus_orbs[1]);
	$('input[name=wd-plus]').val(userTeam.plus_orbs[2]);
	$('input[name=lt-plus]').val(userTeam.plus_orbs[3]);
	$('input[name=dk-plus]').val(userTeam.plus_orbs[4]);
	$('input[name=ht-plus]').val(userTeam.plus_orbs[5]);

	$('input[name=sb-view]').val(userTeam.skill_boost);
	$('input[name=sbr-view]').val(userTeam.skill_bind_resist);
	$('input[name=blr-view]').val(userTeam.blind_resist);
	$('input[name=jmr-view]').val(userTeam.jammer_resist);
	$('input[name=psr-view]').val(userTeam.poison_resist);
	$('input[name=fng-view]').val(userTeam.finger);
	$('#team-block').removeClass('hidden');
}

// Whenever we update the data model for leader skills, also update the UI.
function updateLeaderSkills() {
    // if we haven't created the team yet, wait
    if (!userTeam) {
        setTimeout (updateLeaderSkills, 500);
        return;
    }

    // reset conditionals
    $('select[name="leader1-cond1"]').val("all");
    $('select[name="leader1-cond2"]').val("");
    $('select[name="friend1-cond1"]').val("all");
    $('select[name="friend1-cond2"]').val("");

    if (userTeam.leader_skill) {
        var mult = userTeam.leader_skill[1];
        $('input[name=leader1-field]').val(mult);
        for (var ii = 3; ii < userTeam.leader_skill.length; ii++) {

            var position = ii - 2;
            selectHelper( 'leader1-cond' + position, userTeam.leader_skill[ii][0], userTeam.leader_skill[ii][1] );

        }
    }

    if (userTeam.friend_skill) {
        var mult = userTeam.friend_skill[1];
        $('input[name=friend1-field]').val(mult);
        for (var ii = 3; ii < userTeam.friend_skill.length; ii++) {
            var conditional_type = userTeam.friend_skill[ii][0] == "type" ? "Type" : "Element";

            var position = ii - 2;
            var select = $('select[name=friend1-cond' + position + ']');
            var optgroup = select.find('optgroup[label="' + conditional_type + '"]');
            var option = optgroup.find('option[value="' + userTeam.friend_skill[ii][1] + '"]');

            option.attr('selected', true);
        }
    }
}

function setCombos (value) {
    var existing_combo_rows = $('[id^=combo-row]').length;

    // erase all the existing rows
    for (var ii = 0; ii < existing_combo_rows; ii++)
        $('#minus-combo-btn').click();

    // add the new rows
    for (var ii = 0; ii < value; ii++)
        $('#plus-combo-btn').click();
}

function updateComboColor(comboRow, val) {
    var button = $('#combo-drop-' + comboRow);
    var img = '/images/' + ({0:'fr',1:'wt',2:'wd',3:'lt',4:'dk',5:'ht'})[val] + '.png';

    button.val(val);
    button.find('img').attr('src', img);
    img = img.replace('.', '+.');
    $('.combo-row:nth-of-type(' + comboRow + ')').find('span img').attr('src', img);
}

function setLeaderSkill(leader, type1, type2, value) {
    $('div[id=' + leader + '-skill-override-row-1]').removeClass('hidden');
    $('div[id=' + leader + '-skill-override-row-2]').removeClass('hidden');

    $('select[name=' + leader + '2-cond1]').val(type1);
    if (type2 != undefined)
        $('select[name=' + leader + '2-cond2]').val(type2);
    $('input[name=' + leader + '2-field]').val(value);
    $('input[name=' + leader + '2-field]').change();

}
function changeActiveSkill(value) {
    $("#active-field").val(value);
    var possible_multipliers = $( 'input[name=active-mult]' );
    possible_multipliers.each (function (index, element) {
            if ($(element).val() == value)
                $(element).prop('checked', true);
        });
}

function resetSkillMultipliers() {
    setLeaderSkill ("leader", "", "", 1);
    setLeaderSkill ("friend", "", "", 1);

    changeActiveSkill (1);
    $('select[name=act-cond1]').val("");
    $('select[name=act-cond2]').val("");
}

function selectHelper(field_name, type_or_elem, value) {
    var conditional_type = type_or_elem == "type" ? "Type" : "Element";

    var select = $('select[name=' + field_name + ']');
    var optgroup = select.find('optgroup[label="' + conditional_type + '"]');
    var option = optgroup.find('option[value="' + value + '"]');
    option.attr('selected', true);
}

// TODO: Globals are starting to stack up. If we hit 10, migrating them to some kind of 'game state' data structure may be wise to avoid loose variables.
var awoken_disabled = false;
var coop = false;

var conditional_lead_multiplier = 1;
var conditional_friend_multiplier = 1;
var board = {width: 6, height: 5}

var paddata = new PADData();
var userTeam = null;

$(document).ready( function(){
	jQuery.fn.selectText = function(){
	   var doc = document;
	   var element = this[0];
	   //console.log(this, element);
	   if (doc.body.createTextRange) {
		   var range = document.body.createTextRange();
		   range.moveToElementText(element);
		   range.select();
	   } else if (window.getSelection) {
		   var selection = window.getSelection();        
		   var range = document.createRange();
		   range.selectNodeContents(element);
		   selection.removeAllRanges();
		   selection.addRange(range);
	   }
	};

	paddata.loadData();
	(function clearLoadingBar(){
		if ( paddata.dataLoaded() ){
			$('div.loader').addClass('hidden');
			$('#team-input-methods').removeClass('hidden');
		}
		else { setTimeout(clearLoadingBar, 500); }
	})()

	for (var i = 0; i <= 4; i++){ $('select optgroup[label=Element]').append("<option value=" + i + ">" + elements[i] + "</option>"); } // Populate the Element dropdowns.
	for (var i = 1; i <= 8; i++){ $('select optgroup[label=Type]').append("<option value=" + i + ">" + types[i] + "</option>"); } // Populate the Type dropdowns.
	for (var i = 1; i <= 11; i++){ $('select optgroup[label=Latent]').append("<option value=" + i + ">" + latents[i] + "</option>"); } // Populate the Latent dropdowns.

	$('input[name=active-mult]').on('change', function(e){ $('#active-field').val(e.target.value); }); // Active skill radio buttons.

	// About link handler.
	$('#about-link').on('click', function(e){
		if (!$('#team-block').hasClass('hidden')){
			$('#team-block').fadeToggle();
			$('#results-block').fadeToggle();
			$('#about-block').delay(410).fadeToggle(205);
		}
		else { $('#about-block').fadeToggle(); }
	});

	// Team Input Handler.
	$('#team-id-form').on('submit', function(){
		var id = $("#team-id-form input[name=team-id]").val();
        if (!id){ return false; }

        if (isNaN(id)){
            ind = id.search("/teams/#");
            if (ind == -1){ return; }
            id = id.substr(ind+8);
        }

        $('#about-block').hide();
        $('#manual-entry').hide();
        $.ajax( "https://www.padherder.com/user-api/team/" + id + "/", {dataType: 'json', success: function(a, b, c){
			try { userTeam = new Team(a, paddata); }
			catch (e) {
				paddata.loadDataFromPADHerder();
				(function reloadTeam(){
					if ( paddata.dataLoaded() ){
						userTeam = new Team(a, paddata);
					}
					else { setTimeout(reloadTeam, 500); return false; }
				})()
			}
			(function showTeamTable(){
				if ( userTeam && userTeam.dataLoaded() ){
					buildTable();
				}
				else { setTimeout(showTeamTable, 500); return false; }
			})()
		}});

		return false;
	});
	
        // Handler for manual entry - I tried to refactor the Team Input Handler into a separate function and let it support
        // null input, but couldn't get it to behave properly
        $('a[id=manual-entry]').on('click', function(e){
                $('#about-block').hide();
                $('#manual-entry').hide();
                userTeam = new Team(null, paddata);
                buildTable();
            });

	// Max/Hypermax button handlers.
	$('a[id=normal_toggle]').on('click', function(e){
		for (var i = 0; i < 6; i++){
			userTeam.monsters[i].updateMonster( {'monster': userTeam.monsters[i].original_id, 'current_xp': userTeam.monsters[i].original_xp, 'plus_hp': userTeam.monsters[i].original_pluses.hp,
			'plus_atk': userTeam.monsters[i].original_pluses.atk, 'plus_rcv': userTeam.monsters[i].original_pluses.rcv, 'current_awakening': userTeam.monsters[i].original_awakenings} );
		}
		userTeam.calcTeamStats();
		buildTable();
	});

	// Max button.
	$('a[id=maxlevel_toggle]').on('click', function(e){
		for (var i = 0; i < 6; i++){
			userTeam.monsters[i].updateMonster( {'monster': userTeam.monsters[i].max_evo, 'plus_hp': userTeam.monsters[i].original_pluses.hp, 'plus_atk': userTeam.monsters[i].original_pluses.atk,
			'plus_rcv': userTeam.monsters[i].original_pluses.rcv, 'current_awakening': userTeam.monsters[i].original_awakenings} );
		}
		userTeam.calcTeamStats();
		buildTable();
	});

	// Hypermax button.
	$('a[id=hypermax_toggle]').on('click', function(e){
		for (var i = 0; i < 6; i++){
			userTeam.monsters[i].updateMonster( {'monster': userTeam.monsters[i].max_evo, 'plus_hp': 99, 'plus_atk': 99, 'plus_rcv': 99, 'current_awakening': 50} );
		}
		userTeam.calcTeamStats();
		buildTable();
	});

	// Toggle awoken skills button.
	$('a[id=awoken-toggle]').on('click', function(e){
		awoken_disabled = !awoken_disabled;
		for (var i = 0; i < 6; i++){ userTeam.monsters[i].awake = !awoken_disabled; }
		$('a[id=awoken-toggle]').text( (awoken_disabled) ? "Enable Awoken" : "Disable Awoken" );
		userTeam.calcTeamStats();
		buildTable();
	});

        // Handler for Hera pre-emptive display
        $('a[id=hera-toggle]').on('click', function(e){
                if ($('div[id=hera]').hasClass('hidden'))
                    $('div[id=hera]').removeClass('hidden');
                else
                    $('div[id=hera]').addClass('hidden');
        });

        // Handler for latent toggles
        var latent_selectors = $('[name^=latent-pos]');
        latent_selectors.each ( function (index, element) {
                $(element).on('change', function(e) {

                        // figure out which monster this is, using the last character as the index
                        var monster_index = Number( e.target.name.charAt(e.target.name.length-1));
                        var monster = userTeam.monsters[monster_index];

                        // add the awakening to the object model, pushing out the last one if there are 5
                        if (monster.latents.length == 5)
                            monster.latents.splice (0, 1);

                        monster.latents.push(e.target.value);

                        // recalculate hp/atk/rcv if necessary
                        if (e.target.value < 4 ) {
                            var stat_name = {1: 'hp', 2: 'atk', 3: 'rcv'}[e.target.value];
                            monster[stat_name] = monster.calcStat(stat_name);
                        }

                        // reset the box so we can add another
                        $(element).val("");

                        // update display with recalculated numbers and icons
                        buildTable();
                    });
        });

        // Handler for typical combo setups
        $('select[id=combo-setup]').on('change', function(e){
                resetSkillMultipliers();

                if ($('select[id=combo-setup]').val() == "beelze"){
                    setCombos(6);

                    // set the colors on the first three to dark and the next three to heart
                    updateComboColor(1, 4);
                    updateComboColor(2, 4);
                    updateComboColor(3, 4);
                    updateComboColor(4, 5);
                    updateComboColor(5, 5);
                    updateComboColor(6, 5);

                    // set combos to an optimal 21-9 board
                    var combo_rows = $('div[id^="combo-row"]');
                    combo_rows.each( function (index, element) {
                            if (index < 2) {
                                // find the orb input box and set it
                                var orbs = $(element).find('input[name=orbs]')
                                orbs.val(9);
                                orbs.change();

                                // enhance!
                                $(element).find('input[name=porb]').val(9);

                                // find the row checkbox and check it
                                var row_number = index + 1;
                                $(element).find('div:last input[type=checkbox]').prop("checked", true);
                            }
                            else {
                                $(element).find('input[name=orbs]').val(3);
                                if (index == 2)
                                    $(element).find('input[name=porb]').val(3);
                            }
                    });

                    // set active skill to 1.5x devil
                    selectHelper("act-cond1", "type", 7);
                    changeActiveSkill(1.5);

                    // set conditional leader skill to 1.2x all
                    setLeaderSkill("friend", "all", "", 1.2);
                }

                if ($('select[id=combo-setup]').val() == "freyja"){
                    setCombos(6);

                    // set the colors on the first three to green and the others to heart
                    updateComboColor(1, 2);
                    updateComboColor(2, 2);
                    updateComboColor(3, 2);
                    updateComboColor(4, 5);
                    updateComboColor(5, 5);
                    updateComboColor(6, 5);

                    // set combos to an optimal 21-9 board
                    var combo_rows = $('div[id^="combo-row"]');
                    combo_rows.each( function (index, element) {
                            if (index < 2) {
                                // find the orb input box and set it
                                var orbs = $(element).find('input[name=orbs]')
                                orbs.val(9);
                                orbs.change();

                                // find the row checkbox and check it
                                var row_number = index + 1;
                                $(element).find('div:last input[type=checkbox]').prop("checked", true);
                            }
                            else
                                $(element).find('input[name=orbs]').val(3);
                    });

                    // set active skill to 2x wood/dark
                    selectHelper("act-cond1", "elem", 2);
                    selectHelper("act-cond2", "elem", 4);
                    changeActiveSkill(2);

                    // set conditional leader skill to 1.69x all
                    setLeaderSkill("leader", "all", "", 1.3);
                    setLeaderSkill("friend", "all", "", 1.3);
                }

                if ($('select[id=combo-setup]').val() == "verdandi"){
                    setCombos(6);

                    // set the colors to two green, two red, two heart
                    updateComboColor(1, 2);
                    updateComboColor(2, 2);
                    updateComboColor(3, 0);
                    updateComboColor(4, 0);
                    updateComboColor(5, 5);
                    updateComboColor(6, 5);

                    // set combos to 2 wood TPA, each with 1 enhanced orb, match-3 otherwise
                    var combo_rows = $('div[id^="combo-row"]');
                    combo_rows.each( function (index, element) {
                            if (index < 2) {
                                // find the orb input box and set it
                                var orbs = $(element).find('input[name=orbs]')
                                orbs.val(4);
                                orbs.change();

                                $(element).find('input[name=porb]').val(1);
                            }
                            else
                                $(element).find('input[name=orbs]').val(3);
                    });

                    // set active skill to 2x wood/dark
                    selectHelper("act-cond1", "elem", 2);
                    selectHelper("act-cond2", "elem", 4);
                    changeActiveSkill(2);

                    // set conditional leader skill to all
                    $('select[name=leader2-cond1').val("all");
                    $('select[name=friend2-cond1').val("all");
                }

                if ($('select[id=combo-setup]').val() == "bastet"){
                    setCombos(7);

                    // set the colors on the first six to rainbow
                    for (var ii = 0; ii < 6; ii++) {
                        updateComboColor(ii, ii);
                    }

                    // set combos to 3 with a green TPA
                    var combo_rows = $('input[name=orbs]');
                    combo_rows.each( function (index, element) {
                            if (index == 1)
                                $(element).val(4);
                            else
                                $(element).val(3);
                    });

                    // set the conditional leader skill to 2.25x wood
                    setLeaderSkill("leader", 2, "", 1.5);
                    setLeaderSkill("friend", 2, "", 1.5);
                }

                if ($('select[id=combo-setup]').val() == "shiva"){
                    setCombos(6);

                    // set the colors on the first six to rainbow
                    for (var ii = 0; ii < 6; ii++) {
                        updateComboColor(ii, ii);
                    }

                    // set combos to 3 with a green TPA
                    var combo_rows = $('input[name=orbs]');
                    combo_rows.each( function (index, element) {
                            if (index == 5)
                                $(element).val(4);
                            else
                                $(element).val(3);
                    });

                    // set the conditional leader skill to 9x all
                    setLeaderSkill("leader", "all", "", 3);
                    setLeaderSkill("friend", "all", "", 3);
                }

                if ($('select[id=combo-setup]').val() == "ra"){
                    setCombos(5);

                    // set the colors on the first five to rainbow
                    for (var ii = 0; ii < 5; ii++) {
                        updateComboColor(ii, ii);
                    }

                    // set combos to 3, with a light TPA
                    var combo_rows = $('input[name=orbs]');
                    combo_rows.each( function (index, element) {
                            if (index == 2)
                                $(element).val(4);
                            else
                                $(element).val(3);
                    });

                    // set the conditional leader skill to 4x god/devil
                    setLeaderSkill("leader", 5, 7, 2)
                    setLeaderSkill("friend", 5, 7, 2)
                }

                if ($('select[id=combo-setup]').val() == "yomi"){
                    setCombos(7);

                    // set the colors on the first six to rainbow
                    for (var ii = 0; ii < 6; ii++) {
                        updateComboColor(ii, ii);
                    }

                    // set combos to 3 with a dark match-5
                    var combo_rows = $('input[name=orbs]');
                    combo_rows.each( function (index, element) {
                            if (index == 3)
                                $(element).val(5);
                            else
                                $(element).val(3);
                    });

                    // add a + orb to the dark match-5
                    var combo_plus = $('input[name=porb]');
                    combo_plus.each( function (index, element) {
                            if (index == 3)
                                $(element).val(1);
                    });

                    // set the active skill to 9x dark
                    selectHelper("act-cond1", "elem", 4);
                    changeActiveSkill(9);

                    // set leader skill to "all"
                    $('select[name=leader2-cond1').val("all");
                    $('select[name=friend2-cond1').val("all");
                }
        });

	$('span.editable').attr('contentEditable', true); // Make all the span fields editable.
	$('span.editable').on('focus', function(){ $(this).selectText(); });
	$('span.editable').on('blur', function() {
		if ( $(this).hasClass('level') ) { // Editing level fields
			var level = Number( new RegExp(/[1-9][0-9]|[1-9]/).exec( $(this).text() ) ) || 1;
			var team_no = Number( $(this).parent().attr('class').slice(-1) ) - 1;
			level = Math.min(level, userTeam.monsters[team_no].max_level);
			$(this).text(level);
			userTeam.monsters[team_no].updateMonster({'level': level});
		}

		else if ( $(this).hasClass('plus') ) { // Editing +egg fields
			var eggs = Number( new RegExp(/[1-9][0-9]|[1-9]/).exec( $(this).text() ) ) || 0;
			$(this).text( "(+" + eggs + ")" );
			var team_no = Number( $(this).parent().attr('class').slice(-1) ) - 1;
			var stat = 'plus_' + $(this).parent().parent().attr('id').split('-')[0];
			var dict = {};
			dict[stat] = eggs;
			userTeam.monsters[team_no].updateMonster(dict);
		}

		else if ( $(this).hasClass('awoken') ) { // Editing awoken skill fields
			var awoken = Number( new RegExp(/[1-9][0-9]|[1-9]/).exec( $(this).text() ) ) || 0;
			var team_no = Number( $(this).parent().attr('class').slice(-1) ) - 1;
			awoken = Math.min(awoken, userTeam.monsters[team_no].all_awakenings.length);
			$(this).text( awoken );
			userTeam.monsters[team_no].updateMonster({'current_awakening': awoken});
			userTeam.calcTeamStats();
		}

		else if ( $(this).hasClass('id-box') ) { // Editing monster ID
			var id = Number( new RegExp(/[1-9][0-9]{0,3}/).exec( $(this).text() ) ) || 1;
			var team_no = Number( $(this).parent().attr('class').split(' ')[0].slice(-1) ) - 1; // Split the class string, grab the first one (should probably check it) and grab the sub no. from it.
			var mdata = {'monster': id, 'plus_hp': 0, 'plus_atk': 0, 'plus_rcv': 0, 'current_awakening': 0};
			userTeam.monsters[team_no] = new Monster(mdata, paddata);

                        /*if (!userTeam.monsters[team_no])
                            userTeam.monsters[team_no] = new Monster(null, paddata);

                        userTeam.monsters[team_no].updateMonster(mdata);*/
			userTeam.calcTeamStats();
		}
		buildTable();
	});

	$('#plus-combo-btn').on('click', function(){
		var node = $('.combo-row:last');
		var count = 1;
		if (node.length == 0){ node = $('#combo-label'); }
		else { var count = (node.attr('id').substr(10) * 1) + 1; } // Multiply for int conversion.

		// This is obnoxious.
		var elem = '<div id="combo-row-' + count + '" class="row combo-row">' +
						'<div class="col-md-1 col-md-offset-2">' +
							'<b><p class="form-control-static">Combo ' + count + '</p></b>' +
						'</div>' +
						'<div class="col-md-3">' +
							'<div class="input-group">' +
								'<input class="form-control" name="orbs" type="number" min="3" max="30" step="1" Placeholder="# of Orbs"></input>' +
								'<div class="input-group-btn">' +
									'<button class="btn btn-default dropdown-toggle img-fit" type="button" id="combo-drop-' + count + '" data-toggle="dropdown" value="0">' +
										'<img src="/images/fr.png"/>' +
										'<span class="caret"></span>' +
									'</button>' +
									'<ul class="dropdown-menu" role="menu" aria-labelledby="combo-drop-' + count + '">' +
										'<li value=0 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/fr.png"/></a></li>' +
										'<li value=1 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/wt.png"/></a></li>' +
										'<li value=2 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/wd.png"/></a></li>' +
										'<li value=3 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/lt.png"/></a></li>' +
										'<li value=4 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/dk.png"/></a></li>' +
										'<li value=5 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/ht.png"/></a></li>' +
									'</ul>' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<div class="col-md-1">' +
							'<p class="form-control-static orbs-with">Orbs with</p>' +
						'</div>' +
						'<div class="col-md-3">' +
							'<div class="input-group">' +
								'<input class="form-control" name="porb" type="number" min="0" max="30" step="1" Placeholder="# of +Orbs"></input>' +
								'<span class="input-group-addon img-fit">' +
									'<img src="/images/fr+.png"/>' +
								'</span>' +
							'</div>' +
						'</div>' +
						'<div class="col-md-1 hidden">' +
							'<p class="form-control-static check-row">Row? <input class="pull-right" type="checkbox"></input></p>' +
						'</div>' +
					'</div>';
		node.after(elem);

		// Add handlers for the input fields.
		node = $('.combo-row:last'); // Get the row we just added as the new reference point.
		node.find('input[name=orbs]').on('change', function(e){
			var parent = $(this).parents('div.combo-row');
			if (!parent.length){ return; } // Well, this is awkward.

			var val = $(this).val();
			if (val < 3){
				$(this).val(0);
				return;
			}
			else if (val > (board.width * board.height)){
				val = (board.width * board.height);
				$(this).val(val);
			}

			if ( parent.find('input[name=porb]').val() > val ){ parent.find('input[name=porb]').val(val); } // If there are more +orbs set than orbs, decrease the +orbs value.
			parent.find('input[name=porb]').attr('max', val);

			if (val >= board.width){ parent.find('div:last').removeClass('hidden'); } // If there's a row's worth of orbs, display the row toggle checkbox.
			else {
				parent.find('div:last').addClass('hidden');
				// Now you see me, now you don't... Joke's on you, I'm still around
				// if the value becomes smaller than 6 uncheck the box
				parent.find('div:last input[type=checkbox]').prop("checked", false);
			}
		});
		node.find('li').on('click', function(e){
			var index = $(this).index();
			var button = $(this).parent().parent().children('button');
			var img = ({0:'fr',1:'wt',2:'wd',3:'lt',4:'dk',5:'ht'})[index];
			button.val(index);
			button.find('img').attr('src', ('/images/' + img + '.png'));
			$(this).parents('.combo-row').find('input[name=porb] + span img').attr('src', ('/images/' + img + '+.png'));
		});
	}); // Closes the handler function for the +combo button.

        // Minus combo button handler.
        $('#minus-combo-btn').on('click', function(){
                var node = $('.combo-row:last');
                node.remove();
            });

	// Create one combo row.
	$('#plus-combo-btn').click();


        // Leader skill override toggle handler
        $('a[id=conditional-leader-toggle]').on('click', function(e){
                if ( $('div[id=leader-skill-override-row-1]').hasClass('hidden') ) {
                    $('div[id=leader-skill-override-row-1]').removeClass('hidden');
                    $('div[id=leader-skill-override-row-2]').removeClass('hidden');
                }
                else {
                    $('div[id=leader-skill-override-row-1]').addClass('hidden');
                    $('div[id=leader-skill-override-row-2]').addClass('hidden');
                }
        });

        // Friend skill override toggle handler
        $('a[id=friend-leader-toggle]').on('click', function(e){
                if ( $('div[id=friend-skill-override-row-1]').hasClass('hidden') ) {
                    $('div[id=friend-skill-override-row-1]').removeClass('hidden');
                    $('div[id=friend-skill-override-row-2]').removeClass('hidden');
                }
                else {
                    $('div[id=friend-skill-override-row-1]').addClass('hidden');
                    $('div[id=friend-skill-override-row-2]').addClass('hidden');
                }
        });

        // Handler for leader skill changes
        var override_elements = $('[name^=leader1], [name^=leader2], [name^=friend1], [name^=friend2]');

        override_elements.each ( function (index, element) {
                $(element).on('change', function(e) {

                        // sets the team leader skill in the data model to be one of the two inputted leader skills.
                        // if both rows are used, returns the second row as an extra leader skill.
                        function helper(ident){
                            if ( $('select[name=' + ident + '1-cond1]').val() != "" || $('select[name=' + ident + '1-cond2]').val() != "" &&
                                 $('select[name=' + ident + '2-cond1]').val() != "" || $('select[name=' + ident + '2-cond2]').val() != ""){

                                var leader_skill_set = false;
                                for (var ii = 1; ii <= 2; ii++){

                                    var field_name = ident + ii;
                                    var cond1 = $('select[name=' + field_name + '-cond1]');
                                    var cond2 = $('select[name=' + field_name + '-cond2]');
                                    if ( cond1.val() == "" && cond2.val() == "" ){ continue }

                                    var mult = $('input[name=' + field_name + '-field]');

                                    var skill = [1, +mult.val(), 1];
                                    if (cond1.val() != 'all' && cond1.val() != "") {
                                        skill.push( [$('option:selected', cond1).parent().attr('label') == "Type" ? "type" : "elem", +cond1.val()] );
                                    }
                                    if (cond2.val() != ""){
                                        skill.push( [$('option:selected', cond2).parent().attr('label') == "Type" ? "type" : "elem", +cond2.val()] );
                                    }

                                    if (!leader_skill_set) {
                                        if (ident == "leader") {
                                            userTeam.leader_skill = skill;
                                        }
                                        else
                                            userTeam.friend_skill = skill;

                                        leader_skill_set = true;
                                    }
                                    else
                                        return skill;
                                }
                            }
                        }

                        var skills = [];
                        skills.push(helper('leader'));
                        skills.push(helper('friend'));

                        userTeam.extra_skill = skills;
                        buildTable();
                 });
        });

        // Set resist toggle default
        $('select[id=enemy-resist1]').val(3);
        $('select[id=enemy-resist2]').val(4);

	// Enemy Resist toggle handler.
	$('a[id=enemy-resist-toggle]').on('click', function(e){
		if ( $('label[name=resist-label]').hasClass('hidden') ) {
			$('label[name=resist-label]').removeClass('hidden');
			$('div[name=resist-input]').removeClass('hidden');
			$('a[id=enemy-resist-toggle]').text("Hide");
                        $('input[id=resist-percentage]').val(50);
		}
		else {
			$('label[name=resist-label]').addClass('hidden');
			$('div[name=resist-input]').addClass('hidden');
			$('a[id=enemy-resist-toggle]').text("Resist");
                        $('input[id=resist-percentage]').val(0);
		}
	});

	// Enemy Absorb toggle handler.
	$('a[id=enemy-absorb-toggle]').on('click', function(e){
		if ( $('label[name=absorb-label]').hasClass('hidden') ) {
			$('label[name=absorb-label]').removeClass('hidden');
			$('div[name=absorb-input]').removeClass('hidden');
                        $('select[id=enemy-absorb]').val("all");
			$('a[id=enemy-absorb-toggle]').text("Hide");
		}
		else {
			$('label[name=absorb-label]').addClass('hidden');
			$('div[name=absorb-input]').addClass('hidden');
			$('a[id=enemy-absorb-toggle]').text("Absorb");
                        $('select[id=enemy-absorb]').val("");
                        $('input[id=absorb-threshold]').val("");
		}
	});

        // Handler for sub binds
        var sub_bind_toggles = $('[name^=sub-bind-toggle]');
        sub_bind_toggles.each ( function (index, element) {
                $(element).on('click', function(e) {

                        // figure out which monster this is, using the last character as the index
                        var monster_index = Number( e.target.name.charAt(e.target.name.length-1) ) - 1;

                        // toggle the bind
                        if (userTeam.monsters[monster_index].bound) {
                            userTeam.monsters[monster_index].bound = false;
                            $(element).text("Bind this sub");
                        }
                        else {
                            userTeam.monsters[monster_index].bound = true;
                            $(element).text("Unbind");
                        }

                        // recalculate ATK total
                        $('#dmg-submit').click();
                });
        });

        // Handler for lead binds
        var lead_bind_toggles = $('[name^=lead-bind-toggle]');
        lead_bind_toggles.each ( function (index, element) {
                $(element).on('click', function(e) {

                        // figure out whether this is the player lead or friend lead
                        var is_player_lead = Number( e.target.name.charAt(e.target.name.length-1) ) == 1;

                        function helper (ident) {
                            var monster_index = ident == "leader" ? 0 : 5;

                            // toggle the bind
                            if (userTeam.monsters[monster_index].bound) {
                                userTeam.monsters[monster_index].bound = false;
                                $(element).text("Bind this lead");

                                // restore the leader skill
                                var leader_skill = paddata.getLeaderSkill(userTeam.monsters[monster_index].lead);

                                if (leader_skill)
                                    $('input[name=' + ident + '1-field]').val(leader_skill[1]);

                                $('input[name=' + ident + '2-field]').val(conditional_lead_multiplier);
                                $('input[name=' + ident + '2-field]').change();
                            }
                            else {
                                userTeam.monsters[monster_index].bound = true;
                                $(element).text("Unbind");

                                // remember what the leader skill currently is and reset it
                                conditional_lead_multiplier = $('input[name=' + ident + '2-field]').val();

                                $('input[name=' + ident + '1-field]').val(1);
                                $('input[name=' + ident + '2-field]').val(1);
                                $('input[name=' + ident + '2-field]').change();
                            }

                            // recalculate ATK total
                            $('#dmg-submit').click();
                        };

                        var identifier = is_player_lead ? "leader" : "friend";
                        helper(identifier);
               })
        });


	$('#dmg-submit').on('click', function(){
		// Helper function for element multipliers.
		function getElemMult(elem1, elem2){
			if ((elem1 == 0 && elem2 == 1) || (elem1 == 1 && elem2 == 2) || (elem1 == 2 && elem2 == 0) || (elem1 == 3 && elem2 == 4) || (elem1 == 4 && elem2 == 3)){
				return 2;
			}
			if ((elem2 == 0 && elem1 == 1) || (elem2 == 1 && elem1 == 2) || (elem2 == 2 && elem1 == 0) || (elem2 == 3 && elem1 == 4) || (elem2 == 4 && elem1 == 3)){
				return 0.5;
			}
			return 1;
		}

		// Damage = (Active * Board * Typing * Row Enhance * Attack) - Defense
		// Resist/Absorb happen after defense.
		var active = [1, +$('#active-field').val(), 1];
		var raw_damage = [[0,0], [0,0], [0,0], [0,0], [0,0]]; // Store the basic damage calculated for normal + TPA before multipliers (attack, active, TPA awakenings).
		var total_damage = 0;
		var healing = 0;
		var combos = $('.combo-row');

		var enemy = {'elem': +$('#enemy-elem').val(),
                             'hp': +$('#enemy-health').val(),
                             'curr_hp': +$('#enemy-health').val(),
                             'defense': +$('#enemy-def').val() || 0,
                             'type': [ +$('#enemy-type-1').val(), +$('#enemy-type-2').val(), +$('#enemy-type-3').val() ],
                             'resist': [],
                             'absorb': []};
		if ($('#enemy-resist1').val() || $('#enemy-resist1').val()){
			enemy.resist.push( $('#enemy-resist1').val() );
			enemy.resist.push( $('#enemy-resist2').val() );
			enemy.resist.push( +$('#resist-percentage').val() );
		}
		if ( $('#absorb-threshold').val() || $('#enemy-absorb').val() != 'all' ){
			enemy.absorb.push( $('#enemy-absorb').val() );
			enemy.absorb.push( +$('#absorb-threshold').val() );
		}

		// Active Skill
		if ($('select[name=act-cond1]').val() || $('select[name=act-cond2]').val()){
			// Shove the conditions into the active skill multiplier in the same format as a leader skill.
			var cond = $('select[name=act-cond1]').val();
			if (cond){
				var constraint = $('option:selected', $('select[name=act-cond1]')).parent().attr('label') == "Type" ? "type" : "elem";
				active.push([constraint, +cond]);
			}
			cond = $('select[name=act-cond2]').val();
			if (cond){
				var constraint = $('option:selected', $('select[name=act-cond2]')).parent().attr('label') == "Type" ? "type" : "elem";
				active.push([constraint, +cond]);
			}
		}
		
		// Board
		if (combos.length){ // This should literally never fail unless they consoled stuff out.
			var combo_count = combos.length;
			var rows = [0, 0, 0, 0, 0, 0];
			var tpa_section = [0, 0, 0, 0, 0];
			var colour_contrib = [0, 0, 0, 0, 0, 0];

			for (var i = 1; i <= combos.length; i++){
				var node = $('#combo-row-' + i);
				var combo_size = +node.find('input[name=orbs]').val();
				if (!combo_size){ // If there's no number in the orb field, ignore it, lower the combo count, and move on.
					combo_count--;
					continue;
				}

				var curr_elem = +$('#combo-drop-' + i).val();
				var plus_orbs = +node.find('input[name=porb]').val();
				plus_orbs = plus_orbs > combo_size ? combo_size : plus_orbs; // If there's more +orbs than combo size, then something messed up. Use combo size.

				// Damage Bonuses: 6% per plus orb; 25% per orb in the combo after the first 3; 5% per +orb awakening if the combo contains any plus orbs.
				var dam = (1 + (plus_orbs * 0.06)) * (1 + ((combo_size-3)/4)) * (1 + (userTeam.plus_orbs[curr_elem] * 0.05 * (plus_orbs > 0)));

				// Row and TPA counters.
				rows[curr_elem] += node.find('input[type=checkbox]').prop('checked') ? 1 : 0;
				if (combo_size == 4 && curr_elem != 5){ tpa_section[curr_elem] += dam; } // All non-hearts 4-orb combos get treated like TPAs. Until some other 4-orb mechanics shows up, at least.
				else { colour_contrib[curr_elem] += dam; }
			}
			
			if (!combo_count){
				return false;
			}

			// Row Enhance
			// 25% damage bonus for each extra combo.
			var combo_contrib = 1 + ((combo_count - 1)/4);
			for (var i = 0; i <= 4; i++){
				// bonus from rows - applies to all combos of that color
				var row_contrib = 1 + (0.1 * rows[i] * userTeam.row_enhance[i]);
				var dam_bonus = row_contrib * combo_contrib;

				raw_damage[i] = [dam_bonus * colour_contrib[i], dam_bonus * tpa_section[i]];
			}
			healing = combo_contrib * colour_contrib[5];
		}

                var null_health_warning_flag_required = false;

		// Typing + Attack
                for (var j = 0; j < 2; j++){
                    for (var i = 0; i < 6; i++){
			if (!userTeam.monsters[i] || typeof userTeam.monsters[i].element[j] == 'undefined' || userTeam.monsters[i].bound){
				$('#results-block .team-img-' + (i+1) + ' ~ .dmg-' + (j+1)).text('�');
				continue;
			}
                        var curr_elem = userTeam.monsters[i].element[j];
				
				// If there's no damage for this element, put a 0 and move on.
				if ((raw_damage[curr_elem][0] + raw_damage[curr_elem][1]) == 0){
					$('#results-block .team-img-' + (i+1) + ' ~ .dmg-' + (j+1)).text(0);
					continue;
				}

				var elem_bonus = getElemMult(enemy.elem, curr_elem);
				var attack = userTeam.monsters[i].getStatsUnderLeaders(userTeam.leader_skill, userTeam.friend_skill, userTeam.extra_skill)[1];
				var use_active = userTeam.monsters[i].checkActiveConditions(active, curr_elem);
				var tpa_bonus = Math.pow(1.5, userTeam.monsters[i].countAwakening(27));

				// Sub-element check.
				attack = j == 1 ? (userTeam.monsters[i].element[0] == userTeam.monsters[i].element[1] ? attack * 0.1 : attack / 3) : attack;
				var dam = ( (use_active ? active[1] : 1) *
							(raw_damage[curr_elem][0] + (raw_damage[curr_elem][1] * tpa_bonus)) *
							elem_bonus * attack ) - enemy.defense;

				// Resist
				if (enemy.resist.length && (enemy.resist[0] == 'all' || enemy.resist[0] == curr_elem || enemy.resist[1] == curr_elem)){
					dam *= 1 - (enemy.resist[2]/100);
				}

				dam = Math.max(1, Math.round(dam));

                                // Killer awakenings
                                if (($.inArray(4, enemy.type) != -1 && userTeam.monsters[i].countAwakening(31) > 0) || // dragon killer
                                    ($.inArray(5, enemy.type) != -1 && userTeam.monsters[i].countAwakening(32) > 0) || // god killer
                                    ($.inArray(7, enemy.type) != -1 && userTeam.monsters[i].countAwakening(33) > 0) || // devil killer
                                    ($.inArray(8, enemy.type) != -1 && userTeam.monsters[i].countAwakening(34) > 0)) { // machine killer

                                        dam *= 3;
                                    }

				// Absorb
				if (enemy.absorb.length && (enemy.absorb[0] == 'all' || enemy.absorb[0] == curr_elem)){
					dam *= dam > enemy.absorb[1] ? -1 : 1; // If it breaks the absorb threshhold, absorb it.
				}

                                // figure out how much actual damage we did, given that we cannot go below 0 or above max health
                                var actual_damage = Math.max(enemy.curr_hp - enemy.hp, Math.min(enemy.curr_hp, dam));
                                enemy.curr_hp -= actual_damage;

				total_damage += actual_damage;
				
                                // if the health field was originally blank, absorb may not work correctly. Warn the user if this is the case.
                                if (dam < 0)
                                    null_health_warning_flag_required = true;

                                //console.log("Damage done by monster " + i + ": " + actual_damage + "; Current health: " + enemy.curr_hp);

				$('#results-block .team-img-' + (i+1) + ' ~ .dmg-' + (j+1)).text( dam.toLocaleString() );
			}
		}

                var health_recovered = Math.round($('#rcv-total').text() * healing);
                $('#results-block #health-result').text( health_recovered.toLocaleString() );

		$('#results-block #tdam-result').text( total_damage.toLocaleString() );
		$('#results-block #calc-result').text( enemy.curr_hp == 0 ? 'Spiked! ' + (total_damage - enemy.hp) + ' Overkill!' : enemy.curr_hp.toLocaleString() + " HP" );

		$('#results-block #calc-after').removeClass('hidden');
		$('#results-block #calc-result').removeClass('hidden');
		$('#results-block').removeClass('hidden');

                // This button normally submits the form. Normally we don't want it to, since it will reload the page.
                // However, if the user didn't enter enemy health, we can use the form validation to show a warning,
                // and the validation failure will cause us to stay on the page.
                if (!null_health_warning_flag_required || enemy.hp > 0)
                    return false;
	});

	$('.collapse').collapse();
	
	query_string = function(){
		var query_string = {};
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
			// If first entry with this name
			if (typeof query_string[pair[0]] === "undefined") { query_string[pair[0]] = decodeURIComponent(pair[1]); }
			
			// If second entry with this name
			else if (typeof query_string[pair[0]] === "string") { 
				var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
				query_string[pair[0]] = arr;
			}
			
			// If third or later entry with this name
			else { query_string[pair[0]].push(decodeURIComponent(pair[1])); }
		}
		return query_string;
	}();

	if (query_string.team_id) {
		$("#team-id-form input[name=team-id]").val(query_string.team_id);
		(function loadQueryTeam(){
			if ( paddata.dataLoaded() ){
				$("#team-id-form").trigger('submit');
			}
			else { setTimeout(loadQueryTeam, 500); return false; }
		})();
	}
})