var elements;
var types;
var latents;
var lang;
var lang_obj;

$(document).ready(function(){
	// Load language cookie, or default to EN.
	// 1 week cookie. Might be a bit much.
	var cookie_dur = 604800000;
	lang = "en";
	if (document.cookie){
		var arr = document.cookie.split(';');
		for (var i = 0; i < arr.length; i++){
			var ind = arr[i].indexOf('lang');
			if (ind != -1){
				// Offset to position after lang.
				lang = document.cookie.substr(ind+5, 2);
			}
		}
	}
	// Default to preset text if the language is English, to preserve detailed hyperlinks 'n such.
	if (lang != "en"){
		$.ajax('trans-' + lang + '.json', {'dataType': 'json', success: function(a, b, c){
			for (var x = 0; x < a.length; x++){
				var curr = a[x];
				$(curr.identifier).attr(curr.attr, curr.attr_content);
				$(curr.identifier).text(curr.content);
			}
		}});
	}
	
	if (lang == "en"){
		elements = {0: 'Fire', 1: 'Water', 2: 'Wood', 3: 'Light', 4: 'Dark'};
		types = {0: 'Evo Material', 1: 'Balanced', 2: 'Physical', 3: 'Healer', 4: 'Dragon', 5: 'God', 6: 'Attacker', 7: 'Devil', 8: 'Machine', 12: 'Awoken Material', 13: 'Protected', 14: 'Enhance Material'};
                latents = {1: 'HP', 2: 'ATK', 3: 'RCV', 4: 'Time', 5: 'Heal', 6: 'Fire Resist', 7: 'Water Resist', 8: 'Wood Resist', 9: 'Light Resist', 10: 'Dark Resist', 11: 'Skill Delay Resist'};
		lang_obj = {combo_with: 'Orbs with', combo_placeholder: '# of Orbs', combo_plus_placeholder: '# of +Orbs'};
	}
	else if (lang == "jp"){
		elements = {0: '\u706B', 1: '\u6C34', 2: '\u6728', 3: '\u5149', 4: '\u6697'};
		types = {0: '\u9032\u5316\u7528', 1: '\u30D0\u30E9\u30F3\u30B9', 2: '\u4F53\u529B', 3: '\u56DE\u5FA9', 4: '\u30C9\u30E9\u30B4\u30F3', 5: '\u795E', 6: '\u653B\u6483', 7: '\u60AA\u9B54', 8: '\u30de\u30b7\u30f3', 12: '\u80FD\u529B\u899A\u9192', 13: '\u7279\u5225\u4FDD\u8B77', 14: '\u5F37\u5316\u5408\u6210'};
		lang_obj = {combo_with: '\u30C9\u30ED\u30C3\u30D7\u3068', combo_placeholder: '\u5E7E\u3064\u500B', combo_plus_placeholder: '\u5E7E\u3064\u5F37\u5316\u500B'};
	}
	
	$('#trans-en').on('click', function(){
		$.ajax('trans-en.json', {'dataType': 'json', success: function(a, b, c){
			if (lang == "en") { return; }
			for (var x = 0; x < a.length; x++){
				var curr = a[x];
				$(curr.identifier).attr(curr.attr, curr.attr_content);
				$(curr.identifier).text(curr.content);
			}
			lang = "en";
			
			elements = {0: 'Fire', 1: 'Water', 2: 'Wood', 3: 'Light', 4: 'Dark'};
			types = {0: 'Evo Material', 1: 'Balanced', 2: 'Physical', 3: 'Healer', 4: 'Dragon', 5: 'God', 6: 'Attacker', 7: 'Devil', 12: 'Awoken Material', 13: 'Protected', 14: 'Enhance Material'};
			lang_obj = {combo_with: 'Orbs with', combo_placeholder: '# of Orbs', combo_plus_placeholder: '# of +Orbs'};
			
			exp = new Date();
			exp.setTime( exp.getTime() + cookie_dur );
			document.cookie = "lang=en; expires=" + exp.toUTCString();
		}});
	});
	
	$('#trans-jp').on('click', function(){
		if (lang == "jp") { return; }
		$.ajax('trans-jp.json', {'dataType': 'json', success: function(a, b, c){
			for (var x = 0; x < a.length; x++){
				var curr = a[x];
				$(curr.identifier).attr(curr.attr, curr.attr_content);
				$(curr.identifier).text(curr.content);
			}
			lang = "jp";
			
			elements = {0: '\u706B', 1: '\u6C34', 2: '\u6728', 3: '\u5149', 4: '\u6697'};
			types = {0: '\u9032\u5316\u7528', 1: '\u30D0\u30E9\u30F3\u30B9', 2: '\u4F53\u529B', 3: '\u56DE\u5FA9', 4: '\u30C9\u30E9\u30B4\u30F3', 5: '\u795E', 6: '\u653B\u6483', 7: '\u60AA\u9B54', 12: '\u80FD\u529B\u899A\u9192', 13: '\u7279\u5225\u4FDD\u8B77', 14: '\u5F37\u5316\u5408\u6210'};
			lang_obj = {combo_with: '\u30C9\u30ED\u30C3\u30D7\u3068', combo_placeholder: '\u5E7E\u3064\u500B', combo_plus_placeholder: '\u5E7E\u3064\u5F37\u5316\u500B'};
			
			exp = new Date();
			exp.setTime( exp.getTime() + cookie_dur );
			document.cookie = "lang=jp; expires=" + exp.toUTCString();
		}});
	});
});